//
//  StudySetViewController.swift
//  CS422L
//
//  Created by Luke Norris on 2/16/21.
//

import UIKit

class StudySetViewController: UIViewController {
    var parentVC: FlashCardSetDetailViewController!
    
    @IBOutlet weak var termLabel: UILabel!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var MissedLabel: UILabel!
    @IBOutlet weak var CorrectLabel: UILabel!
    var cards = [Flashcard]()
    var numberOfCardsCorrect: Int = 0
    var numberOfCardsMissed: Int = 0
    var currentIndex: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        cardView.layer.cornerRadius = 8.0
        cardView.layer.borderColor = UIColor.gray.cgColor
        cardView.layer.borderWidth = 2.0
        //connect hard coded collection to sets
        cards = Flashcard.getHardCodedCollection()
        termLabel.text = cards[currentIndex].term
        MissedLabel.text = "Cards Missed: \(numberOfCardsMissed)"
        CorrectLabel.text = "Cards Correct: \(numberOfCardsCorrect)"
        let tap = UITapGestureRecognizer(target: self, action: #selector(StudySetViewController.flipCard))
        termLabel.isUserInteractionEnabled = true
        termLabel.addGestureRecognizer(tap)
    }
    @objc func flipCard(sender: UITapGestureRecognizer) {
        
    }
    
    
    func goToNextCard() {
        currentIndex = currentIndex + 1
        if currentIndex >= cards.count {
            currentIndex = 0
            numberOfCardsMissed = 0
            numberOfCardsCorrect = 0
        }
        MissedLabel.text = "Movies Not Seen: \(numberOfCardsMissed)"
        CorrectLabel.text = "Movies Seen: \(numberOfCardsCorrect)"
        termLabel.text = cards[currentIndex].term
    }
   
    @IBAction func Missed() {
        numberOfCardsMissed = numberOfCardsMissed + 1
        goToNextCard()
    }
    @IBAction func Skip() {
        cards.append(cards[currentIndex])
        cards.remove(at: currentIndex)
        goToNextCard()
    }
    @IBAction func Correct() {
        numberOfCardsCorrect = numberOfCardsCorrect + 1
        goToNextCard()
        print("hey")
    }
}
