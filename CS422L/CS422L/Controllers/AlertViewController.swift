//
//  AlertViewController.swift
//  CS422L
//
//  Created by Luke Norris on 2/16/21.
//

import UIKit

class AlertViewController: UIViewController {
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var cardDefinition: UITextField!
    @IBOutlet weak var cardTerm: UITextField!
    var parentVC: FlashCardSetDetailViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupToLookPretty()
        cardTerm.text = parentVC.cards[parentVC.selectedIndex].term
        cardDefinition.text = parentVC.cards[parentVC.selectedIndex].definition
    }
    
    @IBAction func delete() {
        parentVC.cards.remove(at: parentVC.selectedIndex)
        parentVC.tableView.reloadData()
        self.dismiss(animated: true, completion: {})
    }
    
    @IBAction func edit() {
        parentVC.cards[parentVC.selectedIndex].term = cardTerm.text ?? ""
        parentVC.cards[parentVC.selectedIndex].definition = cardDefinition.text ?? ""
        parentVC.tableView.reloadData()
        self.dismiss(animated: true, completion: {})
    }
    
        // Do any additional setup after loading the view.
    
    func setupToLookPretty()
    {
        alertView.layer.cornerRadius = 8.0
        alertView.layer.borderWidth = 3.0
        alertView.layer.borderColor = UIColor.gray.cgColor
        cardTerm.becomeFirstResponder()
    }

        // Do any additional setup after loading the view.
}
    
